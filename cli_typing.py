#/usr/bin/env python3

import curses
import time
from pygame import mixer
import os
import subprocess as sp
#os.system('mpg123 sounds/a.mp3')


#mixer.pre_init(60000, -16, 2, 2048)
#mixer.init()

screen = curses.initscr()

curses.echo() #
curses.curs_set(0) # invisible mouse cursor
screen.keypad(1)
screen.nodelay(1)

i = 0

sounds_fp = "sounds/{0}.mp3"
command = "mpg123 "+sounds_fp

az = ' abcdefghijklmnoprstuwyz' #23
azi = 0
max_azi = len(az)
curr_string = ''

while  True:
    i += 1

    event = screen.getch()
    screen.clear()

    if event == ord("q"):
        break

    elif event == curses.KEY_UP:
        azi +=1
        if azi == max_azi:
            azi=0
        #mixer.music.load(sounds_fp.format(az[azi]))
        #mixer.music.play()
        #os.system(command.format(az[azi]))
        sp.Popen(command.format(az[azi]), shell=True)

    elif event == curses.KEY_DOWN:
        azi -=1
        if azi == -(max_azi+1):
            azi=0
        #mixer.music.load(sounds_fp.format(az[azi]))
        #mixer.music.play()
        #os.system(command.format(az[azi]))
        sp.Popen(command.format(az[azi]), shell=True)

    elif event == curses.KEY_LEFT:
        #screen.addstr("\nLast letter removed from string.")
        curr_string = curr_string[:-1]
        screen.refresh()
        #os.system(command.format(az[azi]))
        sp.Popen(command.format(az[azi]), shell=True)

    elif event == curses.KEY_RIGHT:
        #screen.addstr("Letter added to string.")
        curr_string += az[azi]
        screen.refresh()
        #os.system(command.format(az[azi]))
        sp.Popen(command.format(az[azi]), shell=True)

    else:
        screen.addstr(curr_string)
        screen.addstr("\nCurrent letter: "+str(az[azi]))
        #screen.addstr("Loop {0}".format(i))

mixer.quit()
curses.endwin()
